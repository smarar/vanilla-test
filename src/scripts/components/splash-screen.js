const loadingElem = document.querySelector('#splash-screen');
export class SplashScreen {

  static show() {
    loadingElem.classList.remove('hide');
  }

  static hide() {
    /*
     * I added 1s because of loading images.
     * TODO: handle loading images then remove loading (or fast loading by using srcset for many sizes)
     */
    setTimeout(async () => {
      await loadingElem.classList.add('hide');
      loadingElem.remove();
    }, 1000);
  }
}
