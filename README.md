# Vanilla card test by SAID MARAR

A small project to build simple cards for testing.


### Installation

```
npm install
```

### Start Dev Server

```
npm start
```

### Build Prod Version

```
npm run build
```

### Features:

* ES6 Support via [babel](https://babeljs.io/) (v7)
* SASS Support via [sass-loader](https://github.com/jtangelder/sass-loader)
* Linting via [eslint-loader](https://github.com/MoOx/eslint-loader)

When you run `npm run build` we use the [mini-css-extract-plugin](https://github.com/webpack-contrib/mini-css-extract-plugin) to move the css to a separate file. The css file gets included in the head of the `index.html`.

### Project:

* Two folders under `src` one for style and script:
    * styles contains `variables`, `functions`, `mixins` and two components:
        * Card component style for posts.
        * Splash screen component style for app loading.
    * scripts contains Card, Splash Screen, App, and index to init app.
    
### TODO:

* Smooth loading after all images loaded.
* To avoid heavy image and also css flex to center it, we should implement srcset to use many sizes of the image.
* Create Model Post and before return the promise response  use a method map to transform data. (ex. `rxjs pipe/map`)

## Perspective

* I wanted to use [Angular](https://angular.io/) but i decided to use [Javascript Native ECMAScript 2018](https://www.ecma-international.org/ecma-262/9.0/index.html)
