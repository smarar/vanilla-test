import { CONFIG } from "../config";

export class ApiService {

  constructor() {
  }

  async getPosts(endpoint, params) {
    return this.get(CONFIG.API.URL + endpoint, params).catch(this.handleError);
  }

  async get(urlStr, params) {
    const url = new URL(urlStr);
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
    return fetch(url).then( response => { return response.json()});
  }

  handleError(e) {
    console.error(e);
  }
}
