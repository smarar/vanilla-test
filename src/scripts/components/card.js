export class Card {
  constructor(options) {
    this.id = options.id;
    this.type = options.type;
    this.title = options.title.rendered;
    this.descriptionHtml = options.excerpt.rendered;
    this.date = new Date(options.date); //2020-02-20T13:22:30;
    this.link = options.link;
    this.author = options['_embedded'].author[0].name;
    this.authorLink = options['_embedded'].author[0].link;
    this.image = options['_embedded']['wp:featuredmedia'][0].source_url;
    this.imageAlt = options['_embedded']['wp:featuredmedia'][0].alt_text;
  }

  render() {
    return `
      <div class="col-4 p-card--highlighted c-card">
        <div class="c-card__header">
          <h5 class="p-muted-heading u-no-margin--bottom">
            Ubuntu
          </h5>
        </div>
        <div class="c-card__content">
          <div class="c-card__img">
            <img src="${this.image}" alt="${this.imageAlt}">
          </div>
          <h4 class="c-card__heading p-heading--four">
            <a href="${this.link}" target="_blank">${this.title}</a>
          </h4>
        </div>
        <em class="c-card__semantic">
          by <a href="${this.authorLink}" target="_blank">${this.author}</a> on ${this.getFormattedDate()}
        </em>
        <div class="c-card__footer">
          ${this.type}
        </div>
      </div>
    `;
  }

  getFormattedDate() {
    const [, monthName, day, year] = this.date.toString().split` `;
    return day + ' ' + monthName + ' ' + year;
  }
}
