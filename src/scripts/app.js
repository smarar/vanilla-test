import { ApiService } from "./services/api-service";
import { Card } from "./components/card";
import { SplashScreen } from "./components/splash-screen";

const ID_APP = '#app';

export class App {
  constructor() {
    this.appDom = document.querySelector(ID_APP);
    this.apiService = new ApiService();
  }

  async init() {
    SplashScreen.show();
    await this.initPosts();
    SplashScreen.hide();
  }

  async initPosts() {
    const posts = await this.apiService.getPosts('posts', {per_page: 3, page: 1, _embed: true});
    const postsContainer = this.appDom.querySelector('.row');
    posts.forEach( post => {
      const card = new Card(post);
      postsContainer.innerHTML += card.render();
    });
    return posts;
  }
}
